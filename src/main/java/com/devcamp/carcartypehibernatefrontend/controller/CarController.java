package com.devcamp.carcartypehibernatefrontend.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.carcartypehibernatefrontend.model.Car;
import com.devcamp.carcartypehibernatefrontend.model.CarType;
import com.devcamp.carcartypehibernatefrontend.repository.ICarRepository;
import com.devcamp.carcartypehibernatefrontend.repository.ICarTypeRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CarController {
    @Autowired
    private ICarRepository carRepository;

    @Autowired
    private ICarTypeRepository carTypeRepository;

    @GetMapping("/cars")
    public ResponseEntity<List<Car>> getAllCar() {
        try {
            List<Car> listCar = new ArrayList<Car>();
            carRepository.findAll().forEach(listCar::add);
            return new ResponseEntity<>(listCar, HttpStatus.NOT_FOUND);

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/types")
    public ResponseEntity<Set<CarType>> SetTypeByCarCode(@RequestParam(name = "carCode") String carCode) {
        try {
            Car fCar = carRepository.findByCarCode(carCode);
            if (fCar != null) {
                return new ResponseEntity<>(fCar.getCarTypes(), HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

            }

        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
