package com.devcamp.carcartypehibernatefrontend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.carcartypehibernatefrontend.model.CarType;

public interface ICarTypeRepository extends JpaRepository<CarType, Long> {

}
