package com.devcamp.carcartypehibernatefrontend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.carcartypehibernatefrontend.model.Car;

public interface ICarRepository extends JpaRepository<Car, Long> {
    Car findByCarCode(String carCode);

}
