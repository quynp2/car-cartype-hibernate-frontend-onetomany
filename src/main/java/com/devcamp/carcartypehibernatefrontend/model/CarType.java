package com.devcamp.carcartypehibernatefrontend.model;

import javax.persistence.*;

@Entity
@Table(name = "type")
public class CarType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long type_id;
    @Column(name = "type_code", unique = true)
    private String typeCode;
    @Column(name = "type_name")
    private String typeName;
    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;

    public CarType() {
    }

    public CarType(long id, String typeCode, String typeName, Car car) {
        this.type_id = type_id;
        this.typeCode = typeCode;
        this.typeName = typeName;
        this.car = car;
    }

    public long getId() {
        return type_id;
    }

    public void setId(long id) {
        this.type_id = type_id;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

}
