package com.devcamp.carcartypehibernatefrontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarCartypeHibernateFrontendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarCartypeHibernateFrontendApplication.class, args);
	}

}
